# mcrx-widget

### Project structure

```
root/
|__public
|__src
|   |__window               # BE srouce
|   |  |__index.ts              # entry
|   |  |__preload.ts            # preload script
|   |__view                 # FE source
|   |  |__... 
|__index.html               # Electron FE entry
|__electron-builder.json    # electron-builder config
```
