import { join } from 'path';
import { release } from 'os';
import { app, BrowserWindow, screen, shell, ipcMain, Tray, Menu } from 'electron';

// Fix Windows 7 black screen
if (release().startsWith('6.1')) app.disableHardwareAcceleration();
if (process.platform === 'win32') app.setAppUserModelId(app.getName());

// Single instance
if (!app.requestSingleInstanceLock()) {
  app.quit();
  process.exit(0);
}

let win: BrowserWindow | null = null;
let tray: Tray | null = null;

// In dist folder
export const ROOT_PATH = {
  dist: join(__dirname, '../..'),
  public: join(__dirname, app.isPackaged ? '../..' : '../../../public'),
}

// Resource paths
const preload = join(__dirname, '../preload/index.js');
const url = process.env.VITE_DEV_SERVER_URL as string;
const indexHtml = join(ROOT_PATH.dist, 'index.html');
const icon = join(ROOT_PATH.public, 'favicon.ico');

// Window entry
async function createWindow() {

  const width = 300;
  const height = 150;
  const margin = 20;

  const area = screen
    .getPrimaryDisplay().workArea;

  win = new BrowserWindow({
    title: 'MonokaiToolkit',
    icon: icon,

    backgroundColor: '#000',
    frame: false,
    // transparent: true,
    resizable: false,
    alwaysOnTop: true,

    // Rottom right of screen
    x: area.x + area.width - width - margin,
    y: area.y + area.height - height - margin,
    width: width,
    height: height,

    webPreferences: {
      preload,
      nodeIntegration: true,
      contextIsolation: false,

      devTools: !app.isPackaged,
    },
  });

  tray = new Tray(icon);
  tray.setTitle(win.title);
  tray.on('double-click', () => win?.show());
  tray.setContextMenu(Menu.buildFromTemplate([
    {
      label: win.title,
      enabled: false
    },
    {
      label: 'Quit',
      click: () => win?.close()
    },
  ]));

  if (app.isPackaged) {
    win.setMenu(null);
    win.loadFile(indexHtml);
  } else {
    win.loadURL(url);
    win.webContents.openDevTools({
      mode: 'undocked'
    });
  }

  win.webContents.on('new-window', (e, url) => {
    e.preventDefault();
    shell.openExternal(url);
  });
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  win = null;
  if (process.platform !== 'darwin') app.quit();
});

app.on('second-instance', () => {
  if (win) {
    if (win.isMinimized()) win.restore();
    win.focus();
  }
});